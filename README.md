# DoItBig - take home assignment

To run the project
```
npm i
npm run start
```

The focus question that I have kept in mind while building, how can I make something both functionality correct and visually attractive?

This code base demostrates nicely how I would go about implementing a store like app, how a component could be implemented in different ways (like desktop vs mobile),
custom pipes, debounce functionality to throttle searches so that the site keeps performant even though someone is rigidly searching, all kinds of store selectors
for combining different store props and the use of angular's environment system.


Be sure to check out mobile as wel.

### Drawbacks
Though I have started to setup Karma to make some tests, the tests are not implemented yet.

The things I wanted to test unit like are
- The forbidden words and its validation
- The request service
- The debounce functionalities
- The store selectors 