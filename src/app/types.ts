
export interface HttpErrorSearch {
    msg: string,
    status?: number
}

export interface HttpResponseSearch {
    data: SearchResult[]
}

export interface SearchResult {
    type: string,
    id: string,
    title: string,
    image_url: string,
    embed_url: string,
    slug: string,
    rating: string,
    trending_datetime: string;
    username: string,
}
