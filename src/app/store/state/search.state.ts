import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { SearchService } from 'app/services/searchService.service';
import { HttpErrorSearch, SearchResult } from 'app/types';
import { Search } from '../actions/search.actions';

export interface SearchStateModel {
    inputDirty: boolean,
    term: string;
    result: SearchResult[],
    searchResultError?: HttpErrorSearch,
    forbiddenSearchWords: string[]
}

@State<SearchStateModel>({
  name: 'search',
  defaults: {
    inputDirty: false,
    term: '',
    result: [],
    searchResultError: undefined,
    forbiddenSearchWords: ['cat', 'pupp']
  }
})

@Injectable()
export class SearchState {

  @Selector()
  static term(state: SearchStateModel) {
    return state.term;
  }

  @Selector()
  static result(state: SearchStateModel) {
    return state.result;
  }

  @Selector()
  static noResultsFound(state: SearchStateModel) {
    return state.inputDirty && state.result.length === 0;
  }

  constructor(
    private searchService: SearchService
  ) {

  }

  @Action(Search.AddSearch) addSeach(
    ctx: StateContext<SearchStateModel>,
    action: Search.AddSearch
  ) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      term: action.searchTerm,
    })
  }

  @Action(Search.SaveSearchResult) saveSearchResult(
    ctx: StateContext<SearchStateModel>,
    action: Search.SaveSearchResult
  ) {

    // Dubble security for validating forbidden words
    if (this.searchService.validateSearch()) {
      const state = ctx.getState();
      ctx.setState({
        ...state,
        inputDirty: true,
        searchResultError: undefined,
        result: action.result
      })
    }
  }

  @Action(Search.SearchResultError) searchResultError(
    ctx: StateContext<SearchStateModel>,
    action: Search.SearchResultError
  ) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      searchResultError: action.error
    })
  }

}
