import { HttpErrorSearch, SearchResult } from "app/types";

export namespace Search {
    export class AddSearch {
        static readonly type = '[DOITBIG] Add Search';
        constructor(public searchTerm: string) {}
    }

    export class SaveSearchResult {
        static readonly type = '[DOITBIG] Save Search Result';
        constructor(public result: SearchResult[]) {}
    }

    export class SearchResultError {
        static readonly type = '[DOITBIG] Search Result Error';
        constructor(public error: HttpErrorSearch) {}
    }
}
