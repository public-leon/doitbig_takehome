import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'topResults'
  })

  export class TopResultsPipe implements PipeTransform {
    transform(collection: any[], topAmount = 3): any[] {
      return collection.slice(0, topAmount);
    }
  }