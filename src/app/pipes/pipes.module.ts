import { NgModule } from '@angular/core';
import { SafePipe } from './safe.pipe';
import { TopResultsPipe } from './top-results.pipe';


@NgModule({
  declarations: [
    TopResultsPipe,
    SafePipe
  ],
  imports: [],
  exports: [
      TopResultsPipe,
      SafePipe
  ],
  providers: [],
  bootstrap: []
})
export class PipesModule { }