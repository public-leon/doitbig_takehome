import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { SearchState } from 'app/store/state/search.state';
import { HttpErrorSearch, SearchResult } from 'app/types';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-collection',
    templateUrl: './collection.component.html',
    styleUrls: ['./collection.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class CollectionComponent implements OnInit {

    @Select(SearchState.noResultsFound) noResultsFound$: Observable<boolean>;
    @Select(state => state.search.searchResultError) searchResultError$: Observable<HttpErrorSearch>;
    @Select(SearchState.result) searchtResult$: Observable<SearchResult[]>;

    constructor() { }

    ngOnInit() {}

}
