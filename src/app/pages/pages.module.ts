import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from 'app/components/components.module';
import { PipesModule } from 'app/pipes/pipes.module';
import { CollectionComponent } from './collection/collection.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule ,
        NgbModule,
        NgbModule,
        ComponentsModule,
        PipesModule
    ],
    declarations: [
        HomeComponent,
        CollectionComponent
    ],
    exports: []
})
export class PagesModule { }
