import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { SearchState } from 'app/store/state/search.state';
import { HttpErrorSearch, SearchResult } from 'app/types';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class HomeComponent {
  simpleSlider = 40;
  doubleSlider = [20, 60];
  state_default = true;
  focus: any;

  @Select(SearchState.result) searchtResult$: Observable<SearchResult[]>;
  @Select(SearchState.noResultsFound) noResultsFound$: Observable<boolean>;
  @Select(state => state.search.searchResultError) searchResultError$: Observable<HttpErrorSearch>;

  constructor(
    private router: Router) {

   }

  showAllClicked(){
    this.router.navigateByUrl('collection');
  }

}
