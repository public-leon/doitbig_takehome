import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { HttpResponseSearch } from "app/types";
import { Observable } from "rxjs";


@Injectable({
    providedIn: 'root',
  })
  export class RequestService {
    constructor(
      private http: HttpClient,
    ) {

    }

    get(url: string, params: HttpParams ): Observable<HttpResponseSearch>{
        const headers = new HttpHeaders({
        'Content-Type': 'application/json',
        });

        return this.http.get<HttpResponseSearch>(url, {
            headers,
            params
        })
    }

}