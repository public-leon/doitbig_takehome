import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { HttpParams} from '@angular/common/http';
import { Store } from '@ngxs/store';
import { Search } from 'app/store/actions/search.actions';
import { RequestService } from './requestService.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SearchService {

  public searchInputSubject: Subject<string> = new Subject<string>();
  public searchSubject: Subject<string> = new Subject<string>();

  constructor(
    private store: Store,
    private requestService: RequestService
  ) {

    this.searchInputSubject.pipe(
      debounceTime(300),
      distinctUntilChanged()
    )
      .subscribe(model => {
        this.store.dispatch(new Search.AddSearch(model));
      });

    this.searchSubject.pipe(
      debounceTime(300),
    )
      .subscribe(() => {
        const searchTerm = this.getCurrentSearchTerm();
        this.searchCollection(searchTerm);
      });
  }

  private searchCollection(searchTerm){
    const params: HttpParams = new HttpParams()
    .append('api_key', environment.apiKey)
    .append('limit', '12')
    .append('q', searchTerm);

    const request = this.requestService.get(environment.apiUrl, params);
    request.subscribe(
          res => {
            const {data} = res;
            this.store.dispatch(new Search.SaveSearchResult(data));
          },
          err => {
            const {error} = err;
            this.store.dispatch(new Search.SearchResultError(error?.meta));
          }
      );
  }

  validateSearch(): boolean{
    const searchTerm = this.getCurrentSearchTerm();

    let forbiddenSearchWords: string[] = [];
    this.store.selectOnce(state => state.search.forbiddenSearchWords).subscribe( value => forbiddenSearchWords = value);

    return !forbiddenSearchWords.find( item => {
      return searchTerm.includes(item)
    });
  }

  searchInputChange(text){
      this.searchInputSubject.next(text);
  }

  search(){
    if (this.validateSearch()){
      this.searchSubject.next();
    } else {
      const searchTerm = this.getCurrentSearchTerm();
      const msg = `Oops, ${searchTerm} is a forbidden word!`
      this.store.dispatch(new Search.SearchResultError({msg}))
    }
  }

  getCurrentSearchTerm(): string {
    let searchTerm = '';
    this.store.selectOnce(state => state.search.term).subscribe( value => searchTerm = value);
    return searchTerm;
  }

}
