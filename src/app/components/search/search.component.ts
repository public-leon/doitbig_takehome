import {Component, Input} from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { SearchService } from 'app/services/searchService.service';
import { Search } from 'app/store/actions/search.actions';
import { SearchState } from 'app/store/state/search.state';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-search-component',
    templateUrl: './search.component.html'
})
export class SearchComponent {
    @Input() isMobileNav = false;

    @Select(SearchState.term) searchTerm$: Observable<string>;

    constructor(
        private searchService: SearchService
    ) {

    }

    searchInputChanged(event){
        this.searchService.searchInputChange(event.target.value)
    }

    searchButtonClick(){
      this.searchService.search();
    }

}
