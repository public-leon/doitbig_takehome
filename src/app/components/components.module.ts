import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { PipesModule } from 'app/pipes/pipes.module';
import { TopResultComponent } from './topresult/topresult.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { SearchResultComponent } from './searchresult/searchresult.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        RouterModule,
        JwBootstrapSwitchNg2Module,
        PipesModule,
        IvyCarouselModule
    ],
    declarations: [
        SearchComponent,
        TopResultComponent,
        SearchResultComponent
    ],
    exports: [ SearchComponent, TopResultComponent, SearchResultComponent ]
})
export class ComponentsModule { }
