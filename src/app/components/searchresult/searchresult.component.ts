import {Component} from '@angular/core';
import { Select } from '@ngxs/store';
import { SearchState } from 'app/store/state/search.state';
import { SearchResult } from 'app/types';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-searchresult-component',
    templateUrl: './searchresult.component.html',
    styleUrls: ['./searchresult.component.scss']
})
export class SearchResultComponent {

    @Select(SearchState.result) searchtResult$: Observable<SearchResult[]>;

    constructor(
    ) {

    }

}
