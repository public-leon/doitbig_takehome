import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopResultComponent } from './topresult.component';

describe('TopResultComponent', () => {
  let component: TopResultComponent;
  let fixture: ComponentFixture<TopResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
