import {Component} from '@angular/core';
import { Select } from '@ngxs/store';
import { SearchState } from 'app/store/state/search.state';
import { SearchResult } from 'app/types';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-topresult-component',
    templateUrl: './topresult.component.html',
    styleUrls: ['./topresult.component.scss']
})
export class TopResultComponent {

    @Select(SearchState.result) searchtResult$: Observable<SearchResult[]>;

    constructor(
    ) {

    }

}
